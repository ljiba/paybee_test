@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        Configure bot defaults
                    </div>
                    <div class="card-body">
                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif
                        <form method="POST" action="{{ route('update-user') }}">
                            @csrf

                            <div class="form-group">
                                <label for="currency" class="col-sm-4 col-form-label text-md-right">Select Currency</label>
                                <?php $userCurrency = ''; ?>
                                @if ($user->currency != null)
                                    <?php $userCurrency = $user->currency; ?>
                                @endif
                                <select id="currency_selector" name="currency">
                                    @foreach($currencies as $key => $val)
                                    <option value="{{ $key }}" @if($key == $userCurrency) selected @endif>{{ $val }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('currency'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('currency') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
