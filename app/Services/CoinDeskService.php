<?php

namespace App\Services;

use GuzzleHttp\Client;
use Exception;

class CoinDeskService
{
    protected $client;

    public function __construct()
    {
        //Initialize guzzle http client
        $this->client = new Client();
    }

    public function btcequivalent($amount, $currency) {

        try {
            $request = $this->client->get('https://api.coindesk.com/v1/bpi/currentprice/'.$currency.'.json');

            if ($request->getStatusCode() == 200) {
                $response = json_decode($request->getBody(), true);

                //Get the value of BTC at given currency
                $rateAtCurrency = $response['bpi'][$currency]['rate_float'];

                //The BTC equivalent for given amount is the amount devided by the rate
                $btcEquivalent = floatval($amount)/floatval($rateAtCurrency);
                return $amount . " " . $currency . " is " . " " . round($btcEquivalent, 6) . " BTC";
            } else {
                return "We could not process your request at this time, please try again later!";
            }
        } catch (Exception $e) {
            return "We could not process your request at this time, please try again later!";
        }
    }

}