<?php

namespace App\Http\Controllers;

use App\Services\CoinDeskService;
use App\UserTelegram;
use BotMan\BotMan\BotMan;
use Illuminate\Support\Facades\Auth;
use App\Currency;
use App\User;

class BotManController extends Controller
{
    /**
     * BotManController constructor.
     * Since we need a way to link a paybee-user to a telegram user we need to ensure users logged in when interacting with bot
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->coindesk = new CoinDeskService();
        $this->userTelegram = new UserTelegram();
    }

    /**
     * Place your BotMan logic here.
     */
    public function handle()
    {
        $botman = app('botman');

        $botman->listen();
    }

    /**
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function tinker()
    {
        return view('tinker');
    }

    /**
     * Fetch the current BTC rate from Coindesk
     * @param float $amount
     * @param string $currency
     */
    public function getBTCEquivalent(BotMan $bot, $amount, $currency) {
        $currencyArray = Currency::pluck('code')->toArray();
        $currency = strtoupper($currency);
        if (! in_array($currency, $currencyArray)) {
            //Use default currency
            //Get the authenticated user and get their currency Code
            $userId = Auth::user()->id;
            $user = User::find($userId);
            if ($user != null) {
                $currency = $user->currency;
                if ($currency != '') {
                    $bot->reply($this->coindesk->btcequivalent($amount, $currency));
                } else {
                    //Use the default currency from the .env file
                    $bot->reply($this->coindesk->btcequivalent($amount, strtoupper(env('DEFAULT_CURRENCY'))));
                }
            }
        } else {
            $bot->reply($this->coindesk->btcequivalent($amount, $currency));
        }
    }

    /**
     * When currency is not supplied use user's selected or default
     * @param BotMan $bot
     * @param string $amount
     * @param string $currency
     */
    public function getBTCByDefaultCurrency(BotMan $bot, $amount, $currency = null) {
        $bot->reply('' . strlen($currency) . '  ' . $currency .'');
        if (strlen($currency) > 0 && (!(is_numeric($currency) || is_float($currency)))) {
            $currency = strtoupper($currency);
            $bot->reply($this->coindesk->btcequivalent($amount, $currency));
        } else {

            $currency = "ZAR";
            $bot->reply('' . strlen($amount) . '' . $amount .'');
            $bot->reply($this->coindesk->btcequivalent($amount, $currency));
        }
    }

    public function linkUserToTelegram(BotMan $bot) {

        //First Check if user already linked
        $userLinked = $this->userTelegram->where('user_id', Auth::user()->id)->first();

        if ($userLinked != null && $userLinked->telegram_id === $bot->getUser()->getId()) {
            $bot->reply("This profile is already linked to telegram id " . $userLinked->telegram_id);
        } else {
            //Go ahead and link the user
            $this->userTelegram->user_id = Auth::user()->id;
            $this->userTelegram->telegram_id = $bot->getUser()->getId();
            $this->userTelegram->save();

            $bot->reply("Your pofile has been linked to telegram id " . $bot->getUser()->getId());
        }
    }


    /**
     * @param BotMan $bot
     * @return string
     */
    public function getPaybeeUser(BotMan $bot) {

        $telegramUser = $this->userTelegram->where('telegram_id', $bot->getUser()->getId())->first();
        if ($telegramUser != null) {
            $bot->reply('' . $telegramUser->user_id .'');
        } else {
            $bot->reply("Looks like there is no user linked to this telegram id :(  - Relink your account to link type /linkProfile");
        }
    }
}
