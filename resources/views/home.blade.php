@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Dashboard
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if(session('user_linked'))
                        <div class="alert alert-success">
                            {!! "Your account is linked to  telegram id ( <strong>" . $user_linked['telegram_id'] . '</strong> )' !!}
                        </div>
                    @else
                        <div class="alert alert-danger">
                            {!! "Your account is not linked to a telegram id, please go to below to link and type  ( /linkProfile ) to link your account" !!} <br />
                            <a href="/botman/tinker">Link Account</a>
                        </div>
                    @endif
                    <a href="bot-config">Set your bot configuration</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
