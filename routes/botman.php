<?php
use App\Http\Controllers\BotManController;
use Illuminate\Support\Facades\Auth;

$botman = resolve('botman');

$botman->hears('/menu', function ($bot) {
    $bot->reply("/getBTCEquivalent \n /linkProfile \n /getUserID");
});

$botman->hears('/getBTCEquivalent {amount} {currency}', BotManController::class.'@getBTCEquivalent');
$botman->hears('/linkProfile', BotManController::class.'@linkUserToTelegram');
$botman->hears('/getUserID', BotManController::class.'@getPaybeeUser');
