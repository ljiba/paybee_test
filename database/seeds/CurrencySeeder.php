<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;

class CurrencySeeder extends Seeder
{
    function __construct()
    {
        $this->client = new Client();
    }

    public function run() {

        $data = $this->client->get('https://api.coindesk.com/v1/bpi/supported-currencies.json');

        if ($data->getStatusCode() == 200) {
            //First truncate table to avoid duplicates
            //Get data and store in db
            $arrayData = json_decode($data->getBody(), true);

            foreach ($arrayData as $dt) {
                DB::table('currencies')->insert([
                    'code' => $dt['currency'],
                    'country' => $dt['country'],
                ]);
            }
        }
    }
}