## Requirements

- Please make sure you are running PHP >= 7.1 and have mysql installed
- Please have ngrok installed - This will allow you to connect to telegram from your local environment

 ## Project Set Up

- Clone the repo 
- Run cp .env.example .env
- Set up your db connection details in the .env file
- Run the below Commands
    - composer install
    - php artisan migrate
    - php artisan db:seed (to populate currencies used in the bot config by the user)
    - php artisan key:generate
    - php artisan serv 
    
#### We need to set up a webhook between our botman installation and telegram API

- After running php artisan serv run [ngrok http 8000 or whatever port your app runs on]()
- Copy the ngrok https forwarding url  - you should have something like (https://api.telegram.org/bot665655907:AAHDYHFETD9LadbWRgkQt3JTRJk09k5pmkI/setWebhook?url=<your ngrok https url>/botman)
- Paste the above url in your web browser to set up a webhook for telegram
- To get Started register
- To view the bot click on the drop down menu and select Go to Chat

Example Commands
- /getBTCEquivalent 30 USD
- /getBTCEquivalent 30 





