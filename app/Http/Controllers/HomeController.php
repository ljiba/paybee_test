<?php

namespace App\Http\Controllers;

use App\Currency;
use App\User;
use App\UserTelegram;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Check if logged in user profile is linked to a telegram profile
        $userHasTelegram = UserTelegram::where('user_id', Auth::user()->id)->first();

        if ($userHasTelegram != null) {
            session()->put('user_linked', true);
            $data['user_linked'] = $userHasTelegram->toArray();
        } else {
            session()->put('user_linked', false);
            $data['user_linked'] = [];
        }

        return view('home', $data);
    }

    public function configBot() {

        //Get a list of all currencies so a user can select
        $userId = Auth::user()->id;
        $data['user'] = User::find($userId);
        $data['currencies'] = Currency::pluck('country', 'code');
        return view('bot_config', $data);
    }

    public function updateUser(Request $request) {

        $request->validate([
            'currency' => 'required'
        ]);

        $user = Auth::user();
        $user->currency = $request->get('currency');
        $user->save();

        session()->flash('success', 'Your settings have been updated!');
        return redirect()->back();
    }
}
